'use strict';

$(document).ready(function () {

    // Rules Swiper
    var rulesSwiper = new Swiper('.rules-swiper', {
        slidesPerView: 2,
        spaceBetween: 50,
        allowTouchMove: false,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        breakpoints: {
            768: {
                slidesPerView: 1,
                allowTouchMove: true,
                spaceBetween: 60
            }
        }
    });

    // Cookies
    function cookies() {
        var cookies = $('.cookies');
        var data = sessionStorage.getItem('cookie');
        if (data === null) {
            cookies.addClass('js-show');
        }

        $('.cookies .btn').on('click', function (e) {
            e.preventDefault();
            cookies.removeClass('js-show');
            sessionStorage.setItem('cookie', 'ok');
        });
    }

    cookies();

    // PopUp
    function popUp() {
        $('.js-popup-button').on('click', function () {
            $('.popup').removeClass('js-popup-show');
            var popupClass = '.' + $(this).attr('data-popupShow');
            $(popupClass).addClass('js-popup-show');
            $('body').addClass('no-scroll');
        });

        closePopup();
    }

    // Close PopUp
    function closePopup() {
        $('.js-close-popup').on('click', function () {
            $('.popup').removeClass('js-popup-show');
            $('body').removeClass('no-scroll');
        });

        $('.popup').on('click', function (e) {
            var div = $(".popup__wrap");
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.popup').removeClass('js-popup-show');
                $('body').removeClass('no-scroll');
            }
        });
    }

    popUp();

    // Input Password
    $('.js-pass').on('click', function () {
        var input = $(this).closest('div').find('input');
        if (input.attr('type') === 'password') {
            input.attr('type', 'text');
        } else {
            input.attr('type', 'password');
        }
    });

    // Select
    $('select').select2();

    // Jquery Validate
    $('form').each(function () {
        $(this).validate({
            ignore: [],
            errorClass: "error",
            validClass: "success",
            errorElement: "div",
            wrapper: "span",
            rules: {
                name: {
                    required: true,
                    normalizer: function normalizer(value) {
                        return $.trim(value);
                    }
                },
                surname: {
                    required: true,
                    normalizer: function normalizer(value) {
                        return $.trim(value);
                    }
                },
                secondname: {
                    required: true,
                    normalizer: function normalizer(value) {
                        return $.trim(value);
                    }
                },
                city: {
                    required: true,
                    normalizer: function normalizer(value) {
                        return $.trim(value);
                    }
                },
                phone: {
                    required: true,
                    phone: true
                },
                email: {
                    required: true,
                    email: true,
                    normalizer: function normalizer(value) {
                        return $.trim(value);
                    }
                },
                password: {
                    required: true
                },
                personalAgreement: {
                    required: true
                },
                rules: {
                    required: true
                },
                captcha: {
                    required: true
                }
            },
            messages: {
                phone: {
                    phone: "Укажите номер телефона"
                },
                email: {
                    email: "Введите правильный email"
                }
            }
        });

        jQuery.validator.addMethod("phone", function (value, element) {
            return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
        });
    });

    // Btn-Disable
    function btnDis() {
        var popup = $('.popup');

        popup.each(function () {
            var validate = $(this).find('input', 'textarea');
            var validateForm = $(this).find('form');
            var btn = $(this).find('.btn');

            validate.on('blur keyup click', function () {
                var checked = $('input:checked').length;
                if (validateForm.valid() && checked) {
                    btn.prop('disabled', false).removeClass('btn_disable');
                }
                if (validateForm.valid()) {
                    btn.prop('disabled', false).removeClass('btn_disable');
                } else {
                    btn.prop('disabled', 'disabled').addClass('btn_disable');
                }
            });
        });
    };

    btnDis();
    // Scroll
    $('.winners__table').scrollbar();

    // Masked Phone
    $("input[type='tel']").mask("+7(999)999-99-99");

    // Burger Menu
    function mobMenu() {
        $('.header__burger').on('click', function () {
            $(this).toggleClass('active');
            $('.nav').toggleClass('active');
            $('.bg-fixed').toggleClass('bg-fixed_active');
            $('body').toggleClass('no-scroll');

            $(document).on('click', function (e) {
                var div = $(".nav, .header__burger");
                if (!div.is(e.target) && div.has(e.target).length === 0) {
                    $('.header__burger').removeClass('active');
                    $('.nav').removeClass('active');
                    $('.bg-fixed').removeClass('bg-fixed_active');
                    $('body').removeClass('no-scroll');
                }
            });
        });
    }
    mobMenu();

    // Paralax
    function parallaxDesk() {
        $('body, html').css('overflow', 'hidden');
        var width = $(window).outerWidth();
        $('body, html').css('overflow', 'visible');

        if (width >= 992) {
            $(window).on('scroll', function () {
                var scrolled = $(window).scrollTop();
                $('.container-img_fruits-first').css('top', 0 + scrolled * .05 + 'px');
                $('.container-img_fruits-second').css('top', 0 + scrolled * .400 + 'px');
            });
        } else {
            $(window).off('scroll');
        }
    };
    parallaxDesk();

    $(window).on('loaded resize', function () {
        parallaxDesk();
    });

    // Click eye
    function clickEye() {
        var open = $('#open-eye');
        var close = $('#close-eye');

        open.on('click', function () {
            open.css('display', 'none');
            close.css('display', 'block');
        });
        close.on('click', function () {
            close.removeAttr('style');
            open.removeAttr('style');
        });
    };

    clickEye();
});